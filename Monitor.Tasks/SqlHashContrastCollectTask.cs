﻿using BSF.Config;
using Monitor.Domain.TimeWatch.Dal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Monitor.Tasks
{
    /// <summary>
    /// Sqlhash对照表采集统计任务
    /// Cron:1 1 23 * * ? 
    /// 描述:包括执行次数过小的（未做sql参数化），及产生sql与hash对照表。
    /// </summary>
    public class SqlHashContrastCollectTask : BSF.BaseService.TaskManager.BaseDllTask
    {
        public override void Run()
        {
            try
            {
                InitConfig();
                Do();
            }
            catch (Exception exception)
            {
                base.OpenOperator.Error("性能监控定时统计SQL性能失败", exception);
            }
        }

        public void InitConfig()
        {
            BSFConfig.MonitorPlatformConnectionString = base.AppConfig["MonitorPlatformConnectionString"];
        }

        private void Do()
        {
            tb_sqlhash_consult_dal Dal = new tb_sqlhash_consult_dal();
            int SqlJudgeBase = Convert.ToInt32(base.AppConfig["SqlJudgeBase"]);
            bool c = Dal.SqlHashContrast(SqlJudgeBase);
            if (c)
            {
                base.OpenOperator.Log("性能监控定时统计SQL性能成功");
            }
        }

        public override void TestRun()
        {
            /*测试环境下任务的配置信息需要手工填写,正式环境下需要配置在任务配置中心里面*/
            base.AppConfig = new BSF.BaseService.TaskManager.SystemRuntime.TaskAppConfigInfo();
            base.AppConfig.Add("SqlJudgeBase", "5");
            base.AppConfig.Add("MonitorPlatformConnectionString", "server=192.168.17.201;Initial Catalog=dyd_bs_monitor_platform_manage;User ID=sa;Password=Xx~!@#;");
            base.TestRun();
        }
    }
}
