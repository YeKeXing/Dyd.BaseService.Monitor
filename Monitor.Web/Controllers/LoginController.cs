﻿using Monitor.Domain.PlatformManage.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Web.Models;
using BSF.Db;
using Monitor.Web.Models;
using BSF.Extensions;
using Monitor.Domain.PlatformManage.Dal;

namespace Web.Controllers
{
    public class LoginController : Controller
    {
        //
        // GET: /Login/

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Login(string appid, string sign, string returnurl)
        {
            var userlogininfo = UserLoginInfo.CurrentUserLoginInfo;
            if (userlogininfo != null)
            {
                return RedirectToAction("index", "Cluster");
            }
            return View();
        }

        //登录
        [HttpPost]
        public ActionResult Login(string appid, string sign, string returnurl, string username, string password, string validate)
        {
            try
            {
                returnurl = returnurl ?? "";
                username = username ?? "";
                password = password ?? "";
                validate = validate ?? "";
                ViewBag.username = username;

                if (System.Configuration.ConfigurationManager.AppSettings["LoginUser"].Contains(";" + username.NullToEmpty() + "," + password.NullToEmpty() + ";"))
                {
                    UserLoginInfo.CurrentUserLoginInfo = new UserLoginInfo() { UserName = username.NullToEmpty() };
                }

                if (UserLoginInfo.CurrentUserLoginInfo != null)
                {
                    #region 6写auth Cookie
                    tb_user_model user = Common.GetUserName(username);
                    if (user == null)
                        throw new Exception("用户已登陆,但该用户未在平台中开权限，请联系管理员添加。");

                    UserLoginInfo.CurrentUserLoginInfo.UserModel = user;

                    #endregion
                    return RedirectToAction("index", "ErrorLog", new { area = "UnityLog", userid = user.id });
                }
                else
                {
                    ModelState.AddModelError("", "用户未登陆成功，请联系管理员在web.config中配置用户，并在平台后台开启权限");
                    return View();
                }
            }
            catch (Exception exp)
            {
                ModelState.AddModelError("", "登陆出错,请咨询管理员。错误信息:" + exp.Message);
                return View();
            }
        }

        //登出
        public ActionResult Logout(string returnurl)
        {
            UserLoginInfo.CurrentUserLoginInfo = null;
            if (string.IsNullOrEmpty(returnurl))
                return Redirect("/Login/Login");
            else
                return Redirect(returnurl);
        }

    }
}
